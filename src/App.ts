import * as bodyParser from "body-parser";
import * as express from "express";
import * as logger from "morgan";
import * as path from "path";

const setupRoutes = require("./api/routes");

// Creates and configures an ExpressJS web server
class App {

  // ref to Express instance
  public express: express.Application;

  // Run constructor methods on the Express instance.
  constructor() {
    this.express = express();
    this.routes();
    this.middlewares();
  }

  // Configure Express middlewares
  private middlewares(): void {
    this.express.use(logger("dev"));
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  // Configure API endpoints
  private routes(): void {
    setupRoutes(this.express);
  }

}

export default new App().express;

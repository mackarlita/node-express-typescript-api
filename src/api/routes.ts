import * as express from "express";
import { NextFunction, Request, Response, Router } from "express";
import ProductsRouter from "./products/products.routes";

module.exports = (app: express.Application) => {

  const router = express.Router();

  /* GET api root. */
  router.get("/api/", (req: Request, res: Response, next: NextFunction) => {
    res.json({
      message: "Hello Karla",
    });
  });

  app.use("/api/products", ProductsRouter);

  app.use("/", router);
};

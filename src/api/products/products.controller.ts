import { NextFunction, Request, Response, Router } from "express";
import * as lodash from "lodash";
import ProductsService from "./products.service";

export class ProductsController {

  /**
   * GET all Products.
   */
  public getAll(req: Request, res: Response, next: NextFunction) {
    ProductsService.getAll()
      .then( (products: any) => {
        res.status(200).json(products);
      })
      .catch( (err: Error) => {
        res.status(400).send(err);
      });
  }

  /**
   * GET Products by Id.
   */
  public getById(req: Request, res: Response, next: NextFunction) {
    const productId: number = parseInt(lodash.get(req, "params.id", null), null);
    ProductsService.getById(productId)
      .then( (product: any) => {
        res.status(200).json(product);
      })
      .catch( (err: Error) => {
        res.status(400).send(err);
      });
  }

}

const productsController = new ProductsController();
export default productsController;

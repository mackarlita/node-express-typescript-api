import * as lodash from "lodash";
import * as Q from "q";

const Products = require("./mock-products.json");

export class ProductsService {

  public getAll(): Q.Promise<any> {
    return Q.resolve(Products);
  }

  public getById(productId: number): Q.Promise<any> {
    if (isNaN(productId)) {
      return Q.reject({
        errorMessage: "Invalid id"
      });
    } else {
      const product = lodash.find(Products, {id: productId});

      if (!product) {
        return Q.reject({
          errorMessage: "Product not found"
        });
      }

      return Q.resolve(product);
    }

  }

}

const productsService = new ProductsService();
export default productsService;

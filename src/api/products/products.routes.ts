import { Router } from "express";
import ProductsController from "./products.controller";

export class ProductsRouter {
  public router: Router;

  /**
   * Initialize the ProductsRouter
   */
  constructor() {
    this.router = Router();
    this.init();
  }

  /**
   * Take each handler, and attach to one of the Express.Router's endpoints.
   */
  public init() {
    this.router.get("/", ProductsController.getAll);
    this.router.get("/:id", ProductsController.getById);
  }

}

// Create the productsRoutes, and export its configured Express.Router
const productsRoutes = new ProductsRouter();
productsRoutes.init();

export default productsRoutes.router;

### **REST API with NodeJS and Express** ###

Written in Typescript


Futures additions:

MySQL with Bookshelf as ORM
Mongo with Mongoose as ODM
...


Commands:


To build the project:

npm build


To start running the server:

npm start


To keep compiling automatically ts files when changes detected:

npm watch


Typescript linter:

npm lint


Run tests:

npm test



API directory (run in browser):

**localhost:3000/api**